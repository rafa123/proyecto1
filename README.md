# SNAKE - GAME

** SNAKE-GAME El juego de la serpiente es un clásico que todos recordamos si hemos tenido un teléfono móvil en las décadas de los 90 o 00.


Puedes jugar el juego aqui <a href = "https://gitlab.com/rafa123/proyecto1">here </a>

Sobre el juego 
-----------------

Una línea formada por cuadros o píxeles se mueve por la pantalla. Si no tienes buenos reflejos, esa línea chocará contra los bordes de la pantalla y perderás la partida. 
Y para obtener puntos, deberás recopilar los cuadros que encuentres a tu paso, lo que a su vez hace más larga esa línea. 
Así se podría definir lo que conocemos como Snake, un juego en el que no está claro si lo protagoniza una serpiente o un gusano.


Lista 
---------

*Agregar el score
* Agregar una serpiente mas al ambiente



Creditos
---------

Famoso juego 2D usando JavaScript puro (https://developer.mozilla.org/es/docs/Games/Workflows/Famoso_juego_2D_usando_JavaScript_puro)


