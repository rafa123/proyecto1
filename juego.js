
let velocidad = 80;
let tamano = 10;
let score=0;
let interval;



class objeto {
constructor(){
    this.tamano=tamano;
}
choque(obj) {  // hacemos la diferencia en x y y con el objeto, en cada una de las posiciones
    let difx = Math.abs(this.x - obj.x);
    let dify = Math.abs(this.y - obj.y);
    if(difx >= 0 && difx < tamano && dify >=0 && dify < tamano){ //tienen que estar pegads
        return true;
    } else {
        return false;
    }
}

}

class Cola extends objeto {  // nuestra serpiente
    constructor(x,y) {
        super();
        this.x = x;    //recibe 
        this.y= y;     //recibe
        this.siguiente = null; // posicion siguiente
    }
    dibujar(ctx) {
        if(this.siguiente != null){
            this.siguiente.dibujar(ctx);
        }
        ctx.fillStyle = "green";   // color de la serpiente
        ctx.fillRect(this.x, this.y, this.tamano,this.tamano); // creamos el cuadrp que forma la serpiente
    }
    setxy(x,y){
        if(this.siguiente != null){
            this.siguiente.setxy(this.x, this.y); // posicion siguiente se almacena en la anterior
        }
        this.x= x;
        this.y= y;
    }
    meter(){
        
        if(this.siguiente == null){
       this.siguiente = new Cola(this.x, this.y);
    } else {
        this.siguiente.meter(); // se agrega la comida que comio
    }
    }
    verSiguiente(){
        return this.siguiente;
    }
}

function gameOver(){
    xdir= 0;
    ydir= 0;       //regresar a su estado original
    ejex= true;   
    ejey= true;
    cabeza = new Cola(20,20);
    comida = new Comida();
    alert("GAME OVER : INTENTA DE NUEVO...");
}
function choquepared(){   // colisiones con paredes
    if(cabeza.x <0 || cabeza.x > 590 || cabeza.y < 0 || cabeza.y > 590){
        gameOver();  //mayor ya que al inicio puede perder
    }
}


function cuerpo(){  // colisiones con cuerpo
    let temp = null;
    try{
        temp = cabeza.verSiguiente().verSiguiente();
 
    }catch(err){
      temp= null;
    }
    while(temp != null)
   if(cabeza.choque(temp)){  // todos los elementos de nuestra serpiente
      gameOver();
   }else {
       temp = temp.verSiguiente();
   }
}



class Comida extends objeto{  //clase de lo que come la serpiente , extiende de objeto
    constructor(){
        super();
        this.x = this.generar();
        this.y = this.generar();
    }
    generar(){
        let numero= (Math.floor(Math.random() *59))*10; //se ubica de forma aleatoria
        return numero;
    }
   colocar(){
       this.x = this.generar(); // agregar posicion aleatoria en x
       this.y = this.generar();
   }
   dibujar(ctx){
       ctx.fillStyle = "red";  //dibujarla
       ctx.fillRect(this.x, this.y, this.tamano,this.tamano);
    
   }


} 


let cabeza =  new Cola(20,20);
let comida = new Comida();
let ejex = true;   //eje x
let ejey = true;
let xdir = 0;       //movimientos
let ydir = 0;

function mov(){
    let nx = cabeza.x + xdir; //nueva posicion 
    let ny = cabeza.y + ydir;
    cabeza.setxy(nx,ny);
}
function control(event){
    let co =event.keyCode;
    if(ejex){
        if(co==38){ //tecla de arriba , nos movemos de 10 en 10
            ydir = -tamano;
            xdir =0;       // no podemos movernos en el mismo eje                          
            ejex = false;  // se desactiva el eje x
            ejey= true;
        }
        if(co==40){
            ydir= tamano;
            xdir = 0;
            ejex= false;
            ejey=true;
        }
    }
    if(ejey){
        if(co==37){
            ydir=0;
            xdir= -tamano;
            ejey=false;
            ejex=true;
        }
        if(co==39){
            ydir=0;
            xdir= tamano;
            ejey=false;
            ejex=true;
        }
    }
}
function dibujar(){
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0,0, canvas.width, canvas.height);
    
   
    cabeza.dibujar(ctx);
    comida.dibujar(ctx);
   
    
  
}

  
//this.score = document.getElementById('score');
/*function drawScore() {
    
   
    setTimeout(() => {
        self.canvas.lockKey = false;
    }, 100);
    this.score.innerText = (self.canvas.score * self.canvas.points).toLocaleString('es-pe');
    
  }*/

function update(){

    cuerpo();
    choquepared();
    dibujar();
    mov();
    if(cabeza.choque(comida)){
         comida.colocar(); //una vez que encuentre la comida , debe generar una nueva posicion aleatoria
    }
    drawScore();

    
    
    
}
setInterval("update()", velocidad);